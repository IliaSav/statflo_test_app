"""Simple statflo test app.

.. moduleauthor:: Savchenko Illia <savchenko@test.com>
.. currentmodule:: sample
.. autofunction:: square
"""

from flask import Flask, Blueprint

from flask.ext.migrate import Migrate
from flask.ext.restless import APIManager


from .config import config

migrate = Migrate()
main = Blueprint('main',__name__)


def create_app(config_name):
    """
    Factory function, creates application object.

    :param config_name: str parametr, set application config settings. Legal value: development, testing.
    :return: Flask application object. Example: app=Flask(__name__).
    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    from .models import db, Lead
    db.init_app(app)

    from .views import auto
    auto.init_app(app)

    apimanager = APIManager(flask_sqlalchemy_db=db)
    apimanager.create_api(Lead, methods=['GET', 'POST', 'PUT', 'DELETE'])

    with app.app_context():
        apimanager.init_app(app)
        migrate.init_app(app, db)
        from . import views, main
        app.register_blueprint(main)

        # Decorate all endpoints with Autodoc.doc():
        for endpoint, function in app.view_functions.items():
            app.view_functions[endpoint] = auto.doc()(function)

        @app.errorhandler(404)
        def not_found(error):
            return "Page not found", 404

        from .docs.views import doc_back, doc_front, RegexConverter
        app.url_map.converters['regex'] = RegexConverter
        app.register_blueprint(doc_back, url_prefix='/docb')
        app.register_blueprint(doc_front, url_prefix='/docf')

    return app

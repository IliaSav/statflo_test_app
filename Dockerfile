# our base image
FROM python:3.4-onbuild

COPY .

# tell the port number the container should expose
EXPOSE 5000

# run the application
CMD ["python", "main.py"]
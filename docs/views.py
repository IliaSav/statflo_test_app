from flask import Blueprint, render_template
from werkzeug.routing import BaseConverter

doc_back = Blueprint('docb', __name__, template_folder='templates')
doc_front = Blueprint('docf', __name__, template_folder='temapltes')


class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]


@doc_back.route('/')
def backend():
    return render_template('backend/index.html')


@doc_back.route('/<path:path>')
def index(path):
    return render_template('backend/{}'.format(path))


@doc_front.route('/')
def frontend_index():
    return render_template('frontend_docs/index.html')


# @doc_front.route('/<regex("\\b[A-Z].*?(.html).*?\\b"):path>')
@doc_front.route('/<path:path>')
def other(path):
    # if ".html" in path:
    return render_template('frontend_docs/{}'.format(path))
    # return doc_front.send_static_file('docf/{}'.format(path))


"""
Module to configure app.
.. module:: config
   :platform: Unix, Windows
   :synopsis: Configuration settings module.


.. moduleauthor:: Savchenko Illia <ilia@statflo.com>
.. currentmodule:: sample
.. autofunction:: square
.. autoclass:: Config
"""

import os
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    """Basic configuration class."""
    Debug = True  # sets debug mode.
    WTF_CSRF_ENABLED = True
    SECRET_KEY = 'ppq-q37(e^r3_e(37-#4u@ag^sra!#netnj(%f*rvym-l2-ow@'
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'migrations')

    @staticmethod
    def init_app(app):
        pass


class DockerConfig(Config):
    """Configuration for docker container."""
    pass


class TestingConfig(Config):
    """
    Class to configure test settings.
    """
    TESTING = True
    SERVER_NAME = '127.0.0.1:5000'
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
                              'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
    WTF_CSRF_ENABLED = False

config = {
    'development': Config,
    'docker': DockerConfig,
    'testing': TestingConfig
    }
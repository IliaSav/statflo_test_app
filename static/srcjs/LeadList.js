import React, { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import LeadStore from './LeadStore.js';
import LeadActions from './LeadActions.js';
import Lead from './LeadComponent.js';
import AddLeadForm from './AddLeadComponent.js';

/**
 * Table with leads inormation.
 */
class LeadList extends Component{
    /**
     * render
     * @param {object} props.leads JSON list of leads with info.
     * @return {ReactElement} markup
     */
    render(){
        let leads = this.props.leads.map((lead)=>
            <Lead key={lead.id} lead={lead}/>
        );
        return(
             <table className="table table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Phone</th>
                        <th>Log</th>
                        <th>Appointment time</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>{leads}</tbody>
             </table>
        )
    }
}

export default LeadList;

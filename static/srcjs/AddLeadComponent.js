import React, { Component, PropTypes } from 'react';
import ReactDOM, { render } from 'react-dom';
import LeadActions from './LeadActions.js'

/**
 * Form ReactJS component for additing leads.
 */
class AddLeadForm extends Component{
    /**
     * AddLeadForm constructor.
     */
    constructor(){
        super(...arguments);
        /**
         * Element state, store data from input fields.
         */
        this.state = {};
    }
    /**
     * render ReactElement <AddLeadForm/>.
     * @return {ReactElement} markup
     */
    render(){
        return(
            <div>
            <form
                onSubmit={this.onSubmit.bind(this)}
                className="form-inline">
                <input
                    ref='name'
                    name='name'
                    className="form-control"
                    type='text'
                    required={true}
                    placeholder='name'
                    value={this.state.name}
                    onChange={this.onFormChange.bind(this)}/>
                <input
                    ref='phone'
                    name='phone'
                    className="form-control"
                    type='text'
                    required={true}
                    placeholder='+1234324234'
                    value={this.state.phone}
                    onChange={this.onFormChange.bind(this)}/>
                <input
                    ref='log'
                    name='log'
                    className="form-control"
                    type='text'
                    required={true}
                    placeholder='info'
                    value={this.state.log}
                    onChange={this.onFormChange.bind(this)}/>
                <input
                    ref='appointment_date'
                    name='appointment_date'
                    className="form-control"
                    type='text'
                    required={true}
                    placeholder='2016-03-17 21:32:00'
                    value={this.state.appointment_date}
                    onChange={this.onFormChange.bind(this)}/>
                <input
                    ref='status'
                    name='status'
                    className="form-control"
                    type='text'
                    required={true}
                    placeholder='lead/profile'
                    value={this.state.status}
                    onChange={this.onFormChange.bind(this)}/>
                <button
                    type='submit'
                    className="btn btn-default"
                    >
                    Add
                </button>
            </form>
            </div>
        )
    }
    /**
     * Change React Element state every time form data changed.
     */
    onFormChange(){
            this.setState({
                name: ReactDOM.findDOMNode(this.refs.name).value,
                phone: ReactDOM.findDOMNode(this.refs.phone).value,
                log: ReactDOM.findDOMNode(this.refs.log).value,
                appointment_date: ReactDOM.findDOMNode(this.refs.appointment_date).value,
                status: ReactDOM.findDOMNode(this.refs.status).value
            })
        }
    /**
     * Create POST request with lead data from form.
     */
    onSubmit(e) {
        e.preventDefault();
        LeadActions.createLead(this.state);
    }
}

export default AddLeadForm;
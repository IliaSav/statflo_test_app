import {Dispatcher} from 'flux';

/**
 * Custom app Dispatcher inherited from flux dispatcher.
 */
class AppDispatcher extends Dispatcher{
    /**
     * Dispatched some action (logs this action to console).
     * @param {function} action Action that was dispatch.
     */
    dispatch(action = {}){
        console.log("Dispatched", action);
        super.dispatch(action);
    }
}

/**
 * Export dispatcher for current application..
 */
export default new AppDispatcher();

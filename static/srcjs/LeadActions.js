import AppDispatcher from './AppDispatcher'
import leadConstants from './constants'

/**
 * Object with Lead component actions.
 */
let LeadActions = {
    reloadLead: function(){
        AppDispatcher.dispatch({
            type: leadConstants.REALOAD_LEADS
        });
    },
    createLead: function(lead){
        AppDispatcher.dispatch({
            type: leadConstants.CREATED_LEAD,
            lead: lead
        });
    },
    editStatus: function(lead){
        AppDispatcher.dispatch({
            type: leadConstants.CONVERT_LEAD_TO_ACCOUNT,
            lead: lead
        })
    }
};

export default LeadActions;
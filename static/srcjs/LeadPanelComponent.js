import React, { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import LeadStore from './LeadStore.js';
import LeadActions from './LeadActions.js';
import Lead from './LeadComponent.js';
import AddLeadForm from './AddLeadComponent.js';
import LeadList from './LeadList.js';

/**
 * Lead Panel represents input form for additing leads and LeadList element.
 */
class LeadPanel extends Component{
    /**
     * constructor
     */
    constructor(){
        super(...arguments);
        this.state = {};
        /**
         * @type {object}
         * Get leads list via get request.
         */
        this.state = LeadStore.getState();
        if(!this.state){
            this.state={"leads":[]};
        }
        LeadActions.reloadLead();
    }
    /**
     * render
     * @return {ReactElement} markup
     */
    render() {
        return (
            <div className='row'>
                <AddLeadForm/>
                <LeadList leads={this.state.leads}/>
            </div>
        )
    }
    _onChange() {
        this.setState(LeadStore.getState());
    }
    /**
     * Invoked immediately before a component is unmounted from the DOM, remove listeners.
     */
    componentWillUnmount() {
        LeadStore.removeChangeListener(this._onChange.bind(this));
    }
    /**
     * Invoked immediately after init render occurs, add listeners.
     */
    componentDidMount() {
        LeadStore.addChangeListener(this._onChange.bind(this));
    }
}

export default LeadPanel;


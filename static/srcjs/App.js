import React from 'react';
import ReactDOM, {render} from 'react-dom';
import LeadPanel from './LeadPanelComponent.js'
import LeadStore from './LeadStore.js'
import LeadActions from './LeadActions.js'

ReactDOM.render(<LeadPanel/>, document.getElementById('root'));

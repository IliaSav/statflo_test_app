var $ = require('jquery');
import {EventEmitter} from 'fbemitter';
import AppDispatcher from  './AppDispatcher'
import leadConstants from './constants'

const CHANGE_EVENT = 'change';
let __emitter = new EventEmitter();

let _state = {
    leads: [],
    message:"",
    addingLead: null
};

let _props = {
    url: '/api/lead'
};

let _reloadLeads = function() {
    $.ajax({
        url: _props.url,
        dataType: 'json',
        cache: false,
        success: function(data) {
            _state.leads = data['objects'];
            console.log("state: "+JSON.stringify(_state.leads));
            LeadStore.emitChange();
        },
        error: function(xhr, status, err) {
            console.error(this.url, status, err.toString());
            _state.leads = [];
            LeadStore.emitChange();
        }
    });
};

let _addLead = function(lead){
    $.ajax({
        url: 'api/lead',
        dataType: 'json',
        headers:{
            'Content-Type': 'application/json'
        },
        method: 'POST',
        data:JSON.stringify(lead),
        cache: false,
        success: function(data) {
            _clearAddingLead();
            _reloadLeads();
        },
        error: function(xhr, status, err) {
            LeadStore.emitChange();
        }
    });
};

let _convertLeadToAccount = function(lead){
    $.ajax({
        url: _props.url+'/'+lead.id,
        dataType: 'json',
        headers:{
            'Content-Type': 'application/json'
        },
        method: 'PUT',
        data:JSON.stringify(lead),
        cache: false,
        success: function(data) {
            _clearAddingLead();
            _reloadLeads();
        },
        error: function(xhr, status, err) {
            LeadStore.emitChange();
        }
    });
};

var _clearAddingLead = function() {
    _state.addingLead = null;
};

let LeadStore = {
    getState: function(){
        console.log("LeadStore state: "+JSON.stringify(_state));
        return _state;
    },
    emitChange: function(){
        __emitter.emit(CHANGE_EVENT);
    },
    addChangeListener: function(callback){
        return __emitter.addListener(CHANGE_EVENT, callback);
    },
    removeChangeListener: function(callback){
        return __emitter.removeEventListener(CHANGE_EVENT, callback);
    }
};

AppDispatcher.register(function(action){
    switch(action.type){
        case leadConstants.CREATED_LEAD:
            _addLead(action.lead);
            break;
        case leadConstants.CONVERT_LEAD_TO_ACCOUNT:
            _convertLeadToAccount(action.lead);
            break;
        case leadConstants.REALOAD_LEADS:
            _reloadLeads();
            break;
    }
    return true;
});

export default LeadStore;

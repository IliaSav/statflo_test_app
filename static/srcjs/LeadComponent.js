import React, { Component, PropTypes } from 'react';
import { render } from 'react-dom';
import LeadStore from './LeadStore.js';
import LeadActions from './LeadActions.js'


/**
 * Table row component with lead information.
 */
class Lead extends Component{
    /**
     * Change lead status to profile.
     */
    editStatus(e){
        e.preventDefault();
        this.props.lead.status = (this.props.lead.status == "profile"?'lead':'profile');
        LeadActions.editStatus(this.props.lead)
    }
    /**
     * render
     * @return {ReactElement} markup
     */
    render(){
        return(
            <tr className="Lead">
                <td className="Lead-name">{this.props.lead.name}</td>
                <td className="Lead-phone">{this.props.lead.phone}</td>
                <td className="Lead-log">{this.props.lead.log}</td>
                <td className="Lead-appointment">{this.props.lead.appointment_date}</td>
                <td className="Lead-status"><a onClick={this.editStatus.bind(this)}>{this.props.lead.status}</a></td>
            </tr>
        )
    }
}


/**
 * @type {object}
 * @property {object} lead lead object
 */
Lead.PropTypes = {
    lead: PropTypes.object
};
//
//class LeadUI extends Component{
//    constructor(){
//        super(...arguments);
//
//    }
//}

export default Lead;
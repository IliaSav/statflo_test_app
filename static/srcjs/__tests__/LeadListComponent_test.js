jest.autoMockOff();
jest.dontMock('../LeadList.js');

var React = require('react');
var ReactDOM = require('react-dom');
var ReactTestUtils = require('react-addons-test-utils');
var expect = require('expect');
var LeadListComponent = require('../LeadList.js').default;

let testLeadList = [
    {
      "appointment_date": "2016-03-17T21:32:00",
      "id": 1,
      "log": "info",
      "name": "name1",
      "phone": "+12312311",
      "status": "lead"
    },
    {
      "appointment_date": "2016-03-18T21:32:00",
      "id": 2,
      "log": "info",
      "name": "name2",
      "phone": "+12311231",
      "status": "lead"
    }
];

describe('LeadListComponent', ()=>{
    it('list with 2 elements', ()=>{
        var leadListComponent = ReactTestUtils.renderIntoDocument(<LeadListComponent leads={testLeadList}/>);
        var leadComponents = ReactTestUtils.scryRenderedDOMComponentsWithClass(leadListComponent, 'Lead');
        var leadNames = ReactTestUtils.scryRenderedDOMComponentsWithClass(leadListComponent, 'Lead-name');

        expect(leadComponents.length).toEqual(testLeadList.length);
        expect(leadNames[0].textContent).toEqual(testLeadList[0].name);
        expect(leadNames[1].textContent).toEqual(testLeadList[1].name);
    });
});
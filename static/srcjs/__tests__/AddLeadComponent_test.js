jest.autoMockOff();
jest.dontMock('../AddLeadComponent.js');

describe('Lead', ()=>{
    var React = require('react');
    var ReactDOM = require('react-dom');
    var ReactTestUtils = require('react-addons-test-utils');
    var expect = require('expect');
    var AddLeadComponent = require('../AddLeadComponent.js').default;

    it('default status is lead', ()=>{
        var addLeadComponent = ReactTestUtils.renderIntoDocument(<AddLeadComponent/>);
        const leadNode = ReactDOM.findDOMNode(addLeadComponent);
        expect(ReactTestUtils.isCompositeComponent(addLeadComponent)).toBeTruthy();
    });
});
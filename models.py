"""
ORM module.
.. module:: models
   :platform: Unix, Windows
   :synopsis: Module contsins ORM models.

.. moduleauthor:: Savchenko Illia <savchenko@test.com>
.. currentmodule:: sample
.. autofunction:: square


"""

import enum

from flask.ext.sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class LeadStatus(enum.Enum):
    """
    Describe lead status=field option.
    """
    lead = "lead"
    profile = "profile"


class Lead(db.Model):
    """
    Describe lead object.
    """
    __tablename__ = 'lead'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    phone = db.Column(db.String(12), unique=True)
    log = db.Column(db.Text)
    appointment_date = db.Column(db.DateTime, unique=True)
    status = db.Column(db.Enum("lead", "profile"))

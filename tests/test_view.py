"""
Module for testing views.

.. moduleauthor:: Savchenko Illia <savchenko@test.com>

.. currentmodule:: sample
.. autofunction:: square

"""

import unittest

from flask import url_for

from app import create_app
from app.models import db, Lead


class AppViewTestCase(unittest.TestCase):
    """
    Simple application's main page test.
    """
    def setUp(self):
        """
        Executes before test running. Set up application settings.
        """
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)
        db.create_all()

    def tearDown(self):
        """
        Executes after tests running.
        """
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_home_page(self):
        """
        Simple test. Checks main page availability.
        """
        response = self.client.get(url_for('main.index'))
        self.assertEqual(response._status_code, 200)
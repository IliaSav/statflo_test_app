"""Module for testing api.

.. moduleauthor:: Savchenko Illia <savchenko@test.com>
.. autofunction:: square
"""

import unittest, json

from flask import url_for

from app import create_app
from app.models import db, Lead


test_item_correct = {'name': 'name1',
                     'phone': '+123456789',
                     'log': 'Log about phone.',
                     'appointment_date': '2016-03-17 21:32:00',
                     'status': "lead"}

test_item_update = {'name': 'name1',
                    'phone': '+123456789',
                    'log': 'Log about phone.',
                    'appointment_date': '2016-03-17 21:32:00',
                    'status': "profile"}


test_item_wrong = {'name': 'name1',
                   'phone': '+123456789',
                   'log': 'Log about phone.',
                   'appointment_date': '2016-03-17 21:32:00',
                   'status': "lead1"}


class AppViewTestCase(unittest.TestCase):
    def setUp(self):
        """
        Executes before test running. Set up application settings.
        """
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)
        db.create_all()

    def tearDown(self):
        """
        Executes after tests running.
        """
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def get_api_headers(self):
        """
        Set up http headers 'Accept' and 'Content-Type'.
        :return: 'Accept': 'application/json',
            'Content-Type': 'application/json'
        """
        return{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }

    def test_lead_get_request(self, n=0):
        """
        Test GET request. url: api/lead/.
        :param n: number of objects returned in response.
        :return: number of objects.
        """
        response = self.client.get(url_for('leadapi0.leadapi'))
        # print(url_for('leadapi0.leadapi'))
        self.assertEqual(response.status_code, 200)
        json_response = json.loads(response.data.decode('utf-8'))
        # print("JSON response: {}".format(json_response))
        objects_number = json_response['num_results']
        self.assertEqual(objects_number, n)
        return objects_number

    def test_lead_create(self):
        """
        Test POST request. url: api/lead/.
        :return: id of created object.
        """
        n = self.test_lead_get_request(0)
        response_correct = self.client.post(url_for('leadapi0.leadapi'),
                                    headers = self.get_api_headers(),
                                    data=json.dumps(test_item_correct))
        self.assertEqual(response_correct.status_code, 201)
        json_response = json.loads(response_correct.data.decode('utf-8'))
        # print("post response {}".format(response_correct.__dict__))
        self.assertEqual(test_item_correct['phone'], json_response['phone'])
        self.test_lead_get_request(n+1)
        response_wrong = self.client.post(url_for('leadapi0.leadapi'),
                                          headers=self.get_api_headers(),
                                          data=json.dumps(test_item_correct))
        self.assertEqual(response_wrong.status_code, 400)
        self.test_lead_get_request(n+1)
        return json_response['id']

    def test_lead_convert_to_profile(self):
        """
        Test PUT request. url: api/lead/<id>. Imitates status changing request.
        """
        item_id = self.test_lead_create()
        response_update = self.client.put(url_for('leadapi0.leadapi')+'/'+str(item_id),
                                           headers=self.get_api_headers(),
                                           data=json.dumps(test_item_update))
        self.assertEqual(response_update.status_code, 200)
        json_response = json.loads(response_update.data.decode('utf-8'))
        self.assertEqual(test_item_update['status'], json_response['status'])
        self.assertEqual(item_id, json_response['id'])

    def test_lead_delete(self):
        """
        Test DELETE request. url: api/lead/<id>.
        """
        item_id = self.test_lead_create()
        n_before = self.test_lead_get_request(1)
        response_delete = self.client.delete(url_for('leadapi0.leadapi')+'/'+str(item_id),
                                           headers=self.get_api_headers())
        self.assertEqual(response_delete.status_code, 204)
        n_after = self.test_lead_get_request()
        self.assertEqual(n_before, n_after+1)
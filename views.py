"""
Views module.
.. module:: view
   :platform: Unix, Windows
   :synopsis: View module (as part of MVC pattern).

.. moduleauthor:: Savchenko Illia <savchenko@test.com>
.. currentmodule:: sample
.. autofunction:: square


"""
import os

from flask import Blueprint, render_template, url_for

from flask.ext.autodoc import Autodoc

from app import main

auto = Autodoc()


@main.route('/', methods=['GET'])
@main.route('/index', methods=['GET'])
def index():
    """Return main page - table with leads list."""
    return render_template("index.html",
                           title='Home')


# @doc.route('/')
# def documentation():
    # return auto.html()

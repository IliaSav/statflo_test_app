app.migrations package
======================

Subpackages
-----------

.. toctree::

    app.migrations.versions

Submodules
----------

app.migrations.manage module
----------------------------

.. automodule:: app.migrations.manage
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: app.migrations
    :members:
    :undoc-members:
    :show-inheritance:
